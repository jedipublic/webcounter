package adapters

import (
	"log"
	"net/http"
	"strconv"

	"gitlab.com/jedipublic/webcounter/usecases"
)

type Handlers struct {
	caseCounter *usecases.CaseCounter
}

func NewHandlers(caseCounter *usecases.CaseCounter) *Handlers {
	return &Handlers{
		caseCounter: caseCounter,
	}
}

func (h Handlers) Counter(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	counter := h.caseCounter.Do()
	_, err := w.Write([]byte(strconv.Itoa(counter)))
	if err != nil {
		log.Printf("http write: %s", err)
	}
	return
}
