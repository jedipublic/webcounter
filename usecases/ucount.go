package usecases

import (
	"time"

	"gitlab.com/jedipublic/webcounter/domain"
)

type cacher interface {
	Add(key domain.Key, ttl time.Time)
	CheckOnlyLastExpiredCache() int
}

type CaseCounter struct {
	defaultTtl time.Duration
	cacher     cacher
}

func New(defaultTtl time.Duration, cacher cacher) *CaseCounter {
	return &CaseCounter{
		defaultTtl: defaultTtl,
		cacher:     cacher,
	}
}

func (c CaseCounter) Do() (result int) {
	c.cacher.Add(struct{}{}, time.Now().Add(c.defaultTtl))
	count := c.cacher.CheckOnlyLastExpiredCache()
	return count
}
