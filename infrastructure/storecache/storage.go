package storecache

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/jedipublic/webcounter/domain"
)

type StoreCache struct {
	filePath string
}

func NewCacheStorage(filePath string) *StoreCache {
	return &StoreCache{
		filePath: filePath,
	}
}

func (s StoreCache) Remove() {
	err := os.Remove(s.filePath)
	if err != nil {
		log.Panicf("save: %s", err)
	}
}

func (s StoreCache) Load() ([]domain.Keyttl, error) {
	var valuez []domain.Keyttl
	rawBytes, err := ioutil.ReadFile(s.filePath)
	if err != nil {
		return nil, err
	}
	text := string(rawBytes)
	lines := strings.Split(text, "\n")
	for _, line := range lines {
		if line == "" {
			continue
		}
		timeParse, err := paresrTime(fmt.Sprintf("%s", line))
		if err != nil {
			return nil, err
		}

		valuez = append(valuez, domain.Keyttl{
			Key: struct{}{},
			Ttl: timeParse,
		})
	}

	return valuez, nil
}

func paresrTime(strTime string) (time.Time, error) {
	i, err := strconv.ParseInt(strTime, 10, 64)
	if err != nil {
		return time.Now(), err
	}
	return time.Unix(i, 0), nil
}

func (s StoreCache) Save(values []domain.Keyttl) error {
	f, err := os.Create(s.filePath)
	if err != nil {
		return err
	}

	defer f.Close()
	for _, value := range values {
		_, err := fmt.Fprintln(f, value.Ttl.Unix())
		if err != nil {
			return err
		}
	}
	return nil
}
