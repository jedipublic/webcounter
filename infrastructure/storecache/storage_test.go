package storecache

import (
	"os"
	"testing"
	"time"

	"gitlab.com/jedipublic/webcounter/domain"
)

func TestStoreCache_SaveLoad(t *testing.T) {
	fileTest := "cache.txt"
	storage := NewCacheStorage(fileTest)
	datasave := []domain.Keyttl{
		{
			Key: struct{}{},
			Ttl: time.Now(),
		},
		{
			Key: struct{}{},
			Ttl: time.Now(),
		},
	}
	err := Save(datasave)
	if err != nil {
		t.Fatalf("save: %s", err)
	}

	data, err := Load()
	if err != nil {
		t.Fatalf("save: %s", err)
	}

	t.Logf("%v", data)

	err = os.Remove(fileTest)
	if err != nil {
		t.Fatalf("save: %s", err)
	}
}
