package httpserv

import (
	"context"
	"fmt"
	"log"
	"net/http"
)

type HttpServe struct {
	httpserve *http.Server
}

type Handlers struct {
	Pattern     string
	HandlerFunc func(w http.ResponseWriter, r *http.Request)
}

func New(port uint, handlers ...Handlers) *HttpServe {
	return &HttpServe{
		httpserve: start(port, handlers...),
	}
}

func (h HttpServe) Shutdown() {
	if err := h.httpserve.Shutdown(context.TODO()); err != nil {
		log.Println(err)
	}
}

func start(port uint, handlers ...Handlers) *http.Server {
	srv := &http.Server{Addr: fmt.Sprintf(":%d", port)}

	for _, handl := range handlers {
		http.HandleFunc(handl.Pattern, handl.HandlerFunc)
	}

	go func() {
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatalf("ListenAndServe(): %s", err)
		}
	}()

	return srv
}
