package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"gitlab.com/jedipublic/webcounter/adapters"
	"gitlab.com/jedipublic/webcounter/infrastructure/httpserv"
	"gitlab.com/jedipublic/webcounter/infrastructure/storecache"
	"gitlab.com/jedipublic/webcounter/service"
	"gitlab.com/jedipublic/webcounter/usecases"
)

const (
	removeCacheTTL = time.Second * 30
	defaultTTL     = time.Second * 60
	cacheFilePath  = "cache.txt"
	httpPort       = 8081
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	osSignal := make(chan os.Signal, 1)
	signal.Notify(osSignal, syscall.SIGTERM, os.Interrupt)

	storeCache := storecache.NewCacheStorage(cacheFilePath)

	defaultCache, err := storeCache.Load()
	if err != nil {
		log.Printf("load cache: %s", err)
	} else {
		storeCache.Remove()
	}

	cacheCounter := service.NewCache(removeCacheTTL, defaultCache)
	caseCountr := usecases.New(defaultTTL, cacheCounter)

	handlerCount := adapters.NewHandlers(caseCountr)

	server := httpserv.New(httpPort, httpserv.Handlers{
		Pattern:     "/",
		HandlerFunc: handlerCount.Counter,
	})

	var wg sync.WaitGroup
	go func() {
		wg.Add(1)
		cacheCounter.Run(ctx)
		wg.Done()
	}()

	<-osSignal
	err = storeCache.Save(cacheCounter.GetElements())
	if err != nil {
		log.Printf("save err: %s", err)
	}

	cancel()
	server.Shutdown()
	wg.Wait()
}
