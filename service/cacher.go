package service

import (
	"context"
	"sort"
	"sync"
	"time"

	"gitlab.com/jedipublic/webcounter/domain"
)

type CacheTimer struct {
	elem []domain.Keyttl
	tick *time.Ticker
	rw   *sync.RWMutex
}

func NewCache(invalidate time.Duration, loadCache []domain.Keyttl) *CacheTimer {
	return &CacheTimer{
		tick: time.NewTicker(invalidate),
		rw:   new(sync.RWMutex),
		elem: loadCache,
	}
}

func (i *CacheTimer) GetElements() []domain.Keyttl {
	i.rw.RLock()
	defer i.rw.RUnlock()
	return i.elem
}

func (i *CacheTimer) Add(key domain.Key, ttl time.Time) {
	i.rw.Lock()
	defer i.rw.Unlock()
	i.elem = append(i.elem, domain.Keyttl{Ttl: ttl, Key: key})
	sort.Sort(i)
}

func (i *CacheTimer) Run(ctx context.Context) {
	for {
		select {
		case <-i.tick.C:
			_ = i.CheckOnlyLastExpiredCache()
		case <-ctx.Done():
			return
		}
	}
}

func (i *CacheTimer) CheckOnlyLastExpiredCache() int {
	i.rw.Lock()
	defer i.rw.Unlock()
	for _, e := range i.elem {
		if e.Ttl.After(time.Now()) {
			return len(i.elem)
		}

		if e.Ttl.Before(time.Now()) {
			i.elem = i.elem[1:]
		}
	}
	return len(i.elem)
}

func (i *CacheTimer) Len() int {
	return len(i.elem)
}

func (i *CacheTimer) Swap(x, y int) {
	i.elem[x], i.elem[y] = i.elem[y], i.elem[x]
}

func (i *CacheTimer) Less(x, y int) bool {
	return i.elem[x].Ttl.Before(i.elem[y].Ttl)
}
