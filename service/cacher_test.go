package service

import (
	"sort"
	"sync"
	"testing"
	"time"

	"gitlab.com/jedipublic/webcounter/domain"
)

func TestInvalidator_CheckOnlyLastExpiredCache(t *testing.T) {
	firstTime := time.Now().Add(time.Nanosecond)
	secondTime := time.Now().Add(time.Minute)
	invalidator := &CacheTimer{rw: new(sync.RWMutex)}
	invalidator.elem = append(invalidator.elem, domain.Keyttl{Ttl: firstTime, Key: struct{}{}})
	invalidator.elem = append(invalidator.elem, domain.Keyttl{Ttl: secondTime, Key: struct{}{}})
	sort.Sort(invalidator)

	<-time.After(time.Millisecond)
	gotLen := invalidator.CheckOnlyLastExpiredCache()
	if gotLen != 1 {
		t.Errorf("got len = %d, but want len = 1", gotLen)
	}
}

func TestSort(t *testing.T) {
	firstTime := time.Now()
	secondTime := time.Now().Add(time.Minute)
	invalidator := &CacheTimer{rw: new(sync.RWMutex)}
	invalidator.elem = append(invalidator.elem, domain.Keyttl{Ttl: firstTime, Key: struct{}{}})
	invalidator.elem = append(invalidator.elem, domain.Keyttl{Ttl: secondTime, Key: struct{}{}})
	sort.Sort(invalidator)
	if invalidator.elem[0].Ttl == secondTime {
		t.Errorf("Got %v != want %v", invalidator.elem[0].Ttl, secondTime)
	}
}
